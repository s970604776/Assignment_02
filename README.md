# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 
    - Level：當你消滅場上所有怪物時，會自動生成下一波的怪物。分數越高（>1000），怪物越強，還有幾率出現BOSS。
    - Skill：畫面左上角顯示可使用技能次數。按下空白鍵可以使用技能，此技能能夠清除敵人的子彈，和造成敵人大量出血。
2. Animations : 
    - 玩家和敵人都有動畫。
3. Particle Systems : 
    - 子彈碰到玩家或敵人時有出血效果。
    - 玩家死亡 / 敵人死亡 有粒子爆炸效果。
4. Sound effects : 
    - 背景音樂｜按鈕音效｜擊中敵人音效。
    - 在選單上可以調音樂大小。
5. Leaderboard : 
    - 沒有。

# Bonus Functions Description : 
1. Multi-playe : 
    - 線下雙打模式。
    - 玩家1 { 上下左右 }
    - 玩家2 { WASD }
2. Little helper :
    - 到達一定分數時（1000），給你一個小幫手，它在你身旁幫助你消滅敵人。
3. Bullet automatic aiming : 
    - 小幫手的子彈會追蹤敵人。
4. 一般敵人：
    - 有獨特的移動方式。
5. BOSS：
    - 有獨特的子彈軌跡。
