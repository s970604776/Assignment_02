var play = {
    render: function(){
        //game.debug.body(this.player);
    },

    create: function(){
        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.background = game.add.tileSprite(0, 0, game.width*2, game.height*2,'background');
        this.background.scale.setTo(0.5,0.5);

        this.playerSetting();
        this.bulletSetting();
        this.enemySetting();
        this.createBoss();

        this.cursor = game.input.keyboard.createCursorKeys();
        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        this.AKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.SKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
        this.DKey = game.input.keyboard.addKey(Phaser.Keyboard.D);
        this.WKey = game.input.keyboard.addKey(Phaser.Keyboard.W);

        game.scores = 0;
        game.scoresText = game.add.text(10, 10, "scores: " + game.scores.toString(), {font:'15px',fill:'#ffffff'});
        this.skillText = game.add.text(10, 30, "skill: " + 3, {font:'15px',fill:'#ffffff'});
        this.damageText = game.add.text(10, 50, "damage: " + 3, {font:'15px',fill:'#ffffff'});

        if(game.gamemode == 1)this.player2 = 0;
    },

    update: function(){
        this.backgroundMove();
        this.playerMove();
        this.playerHPcontrol();
        this.bulletControl();
        this.enemyMove();
        this.bossMove();

        this.damageText.setText('damage: '+ this.player.damage.toString());
        this.skillText.setText('skill: '+ this.player.skillnum.toString());

        if(game.gamemode == 2){
            game.physics.arcade.collide(this.player, this.player2);
        }

        this.escKey.onDown.add(()=>{
            game.paused = true;
        })

        
    },
    paused: function(){
        if(!this.exitBtn){
            this.exitBtn = game.add.button(
                game.width/2, 
                game.height* 4/10, 
                'button',
                ()=>{
                    game.btnAudio.play();
                    game.paused = false;
                    
                    this.exitBtn.destroy();
                    this.backBtn.destroy();
                    this.exitBtn = null;
                    this.backBtn = null;

                    game.state.start("menu");
                },
                this, 7, 6, 7
            );

            this.exitBtn.anchor.setTo(0.5,0.5);
        }else{
            this.exitBtn.visible = 1;
        }

        if(!this.backBtn){
            this.backBtn = game.add.button(
                game.width/2, 
                game.height* 6/10, 
                'button',
                ()=>{
                    game.btnAudio.play();
                    this.backBtn.visible = 0;
                    this.exitBtn.visible = 0;

                    game.paused = false;
                },
                this, 9, 8, 9
            );

            this.backBtn.anchor.setTo(0.5,0.5);
        }else{
            this.backBtn.visible = 1;
        }

    },

    pauseUpdate: function(){
        this.escKey.onDown.add(()=>{
            this.backBtn.visible = 0;
            this.exitBtn.visible = 0;

            game.paused = false;
        })
    },

    backgroundMove: function(){
        this.background.tilePosition.y += 1;
    },

    //player
    playerSetting: function(){
        this.player = game.add.sprite(game.width/2 + 30, game.height - 80, 'player');
        this.player.anchor.setTo(0.5,0.5);
        this.player.animations.add('player_move',[0,1,2,1,0], 8, true);

        this.player.hp = 1000;
        this.player.maxhp = 1000;
        this.player.damage = 10;
        this.player.skillnum = 3;
        this.player.skill = [];

        this.player.hpBar = game.add.sprite(this.player.x, this.player.y + this.player.height/2, 'hp');
        this.player.hpBar.anchor.setTo(0.5,0.5);
        this.player.hpBar.scale.setTo(1.2,0.1); 

        game.physics.enable(this.player, Phaser.Physics.ARCADE);

        this.player.body.collideWorldBounds = true;
        this.player.body.setSize(20, 20, (this.player.width - 20)/2, (this.player.height - 20)/2);

        if(game.gamemode == 2){
            this.player2 = game.add.sprite(game.width/2 - 30, game.height - 80, 'player');
            this.player2.anchor.setTo(0.5,0.5);
            this.player2.animations.add('player_move',[0,1,2,1,0], 8, true);
    
            this.player2.hp = 1000;
            this.player2.maxhp = 1000;
            this.player2.damage = 10;
            this.player2.skillnum = 3;
            this.player2.skill = [];
    
            this.player2.hpBar = game.add.sprite(this.player.x, this.player.y + this.player.height/2, 'hp');
            this.player2.hpBar.anchor.setTo(0.5,0.5);
            this.player2.hpBar.scale.setTo(1.2,0.1); 
    
            game.physics.enable(this.player2, Phaser.Physics.ARCADE);
    
            this.player2.body.collideWorldBounds = true;
            this.player2.body.setSize(20, 20, (this.player.width - 20)/2, (this.player.height - 20)/2);
        }

    },

    playerMove: function(){
        if(!this.player.killed){
            this.player.animations.play('player_move');
            this.player.body.velocity.setTo(0,0);
    
            if(this.cursor.up.isDown){
                this.player.body.velocity.y = -150;
            }else if(this.cursor.down.isDown){
                this.player.body.velocity.y = 150;
            }
    
            if(this.cursor.left.isDown){
                this.player.body.velocity.x = -150;
            }else if(this.cursor.right.isDown){
                this.player.body.velocity.x = 150;
            }
    
            if(this.spaceKey.isDown){

                if(this.player.skillnum > 0 && this.spaceKey.once == true){
                    this.spaceKey.once = false;
                    this.player.skillnum--;

                    var create = true;
                    for(let i=0; i<this.player.skill.length; i++){
                        if(this.player.skill[i].x == -1000){
                            this.player.skill[i].x = this.player.x;
                            this.player.skill[i].y = this.player.y;
                            this.player.skill[i].body.velocity.y = -300;
                            create = false;
                            break;
                        }
                    }

                    if(create){

                        var skill = game.add.sprite(this.player.x,this.player.y,'hp');
                        skill.scale.setTo(20,0.1);
                        skill.anchor.setTo(0.5,0.5);

                        game.physics.enable(skill, Phaser.Physics.ARCADE);
                        
                        skill.body.velocity.y = -300;
                        this.player.skill.push(skill);
                    }

                }
            }
            if(this.spaceKey.isUp){
                this.spaceKey.once = true;
            }
        }
        if(game.gamemode == 2){
            if(!this.player2.killed){
                this.player2.animations.play('player_move');
                this.player2.body.velocity.setTo(0,0);
        
                if(this.WKey.isDown){
                    this.player2.body.velocity.y = -150;
                }else if(this.SKey.isDown){
                    this.player2.body.velocity.y = 150;
                }
        
                if(this.AKey.isDown){
                    this.player2.body.velocity.x = -150;
                }else if(this.DKey.isDown){
                    this.player2.body.velocity.x = 150;
                }
            }
        }
        else{
            if(this.player2){
                this.player2.animations.play('player_move');
                this.player2.x = this.player.x + this.player.width;
                this.player2.y = this.player.y + 20;
            }
        }

        if(game.time.now > this.bulletTime){
            
            if(!this.player.killed)
                if(!this.recycleBullet(this.player, 0, 0, -300, this.player.damage)){
                    this.player.bullets.push(this.createBullet(this.player, 0, 0, -300, this.player.damage));
                }
            if(game.gamemode==2){
                if(!this.player2.killed)
                    if(!this.recycleBullet(this.player2, 0, 0, -300, this.player2.damage)){
                       this.player.bullets.push(this.createBullet(this.player2, 0, 0, -300, this.player2.damage));
                    }
            }
            else{
                if(this.player2){
                    for(var i=0; i<this.enemys.length; i++){
                        var enemy = this.enemys[i];
                        if(!enemy.killed){
                            var x = enemy.x - this.player2.x;
                            var y = enemy.y - this.player2.y;

                            if(!this.recycleBullet(this.player2, 0, x/2, y/2, this.player.damage)){
                                this.player.bullets.push(this.createBullet(this.player2, 0, x/2, y/2, this.player.damage));
                            }
                            break;
                        }
                    }
                    if(!this.boss.killed){
                        var x = this.boss.x - this.player2.x;
                        var y = this.boss.y - this.player2.y;
                        if(!this.recycleBullet(this.player2, 0,x/2 , y/2, this.player.damage)){
                            this.player.bullets.push(this.createBullet(this.player2, 0, x/2, y/2, this.player.damage));
                        }
                    }
                }
            }

            this.bulletTime = game.time.now + 300;
        }
    },

    playerHPcontrol: function(){
        //position
        if(!this.player.killed){
            this.player.hpBar.x = this.player.x;
            this.player.hpBar.y = this.player.y + this.player.height/2;
    
            var hp = this.player.hp/this.player.maxhp * 1.2;
            hp = (hp<0)? 0:hp;
    
            this.player.hpBar.scale.setTo(hp, 0.1);
    
            if(hp <= 0){
                this.player.body.collideWorldBounds = false;
                this.player.y = -1000;
                this.player.killed = true;
                setTimeout(()=>{
                    if(game.gamemode == 1)
                        game.state.start('endGame');
                    else if(this.player2.killed)
                        game.state.start('endGame');
                },3000);
            }
        }


        if(game.gamemode == 2 && !this.player2.killed){
            this.player2.hpBar.x = this.player2.x;
            this.player2.hpBar.y = this.player2.y + this.player2.height/2;
    
            var hp = this.player2.hp/this.player2.maxhp * 1.2;
            hp = (hp<0)? 0:hp;
    
            this.player2.hpBar.scale.setTo(hp, 0.1);
    
            if(hp <= 0){
                this.player2.kill();
                this.player2.killed = true;
                setTimeout(()=>{
                    if(this.player.killed)
                        game.state.start('endGame');
                },3000);
            }
        }
    },

    //bullet
    bulletSetting: function(){
        this.player.bullets = [];
        this.bulletTime = 0;
    },

    createBullet: function(person, type, bulletVelocityX, bulletVelocityY, damage){
        // type = 0 : player
        // type = 1 : enemy
        if(type == 0){ 
            var bullet = game.add.sprite(person.x, person.y - person.height/2, 'playerBullet');
            bullet.anchor.setTo(0.5,0.5);
            bullet.scale.setTo(0.2,0.2);
        }
        else{
            var bullet = game.add.sprite(person.x, person.y + person.height/2, 'enemyBullet');
            bullet.anchor.setTo(0.5,0.5);
            bullet.scale.setTo(0.2,0.2);
        }
    
        game.physics.enable(bullet, Phaser.Physics.ARCADE);
    
        bullet.body.velocity.y = bulletVelocityY;
        bullet.body.velocity.x = bulletVelocityX;

        bullet.damage = damage;

        return bullet;
    },

    recycleBullet: function(person, type, bulletVelocityX, bulletVelocityY, damage){
        var isRecycled = false;
        
        if(type == 0){
            for(var i=0; i < this.player.bullets.length; i++){
                var bullet = this.player.bullets[i];
                if(bullet.x == -1000){
                    bullet.x = person.x;
                    bullet.y = person.y - person.height/2;
                    bullet.body.velocity.x = bulletVelocityX;
                    bullet.body.velocity.y = bulletVelocityY;
                    bullet.damage = damage;
                    isRecycled = true;
                    break;
                }
            }
        }
        else{
            for(var i=0; i < this.enemyBullets.length; i++){
                var bullet = this.enemyBullets[i];
                if(bullet.x == -1000){
                    bullet.x = person.x;
                    bullet.y = person.y + person.height/2;
                    bullet.body.velocity.x = bulletVelocityX;
                    bullet.body.velocity.y = bulletVelocityY;
                    bullet.damage = damage;
                    isRecycled = true;
                    break;
                }
            }
        }

        return isRecycled;
    },

    bulletControl: function(){
        //player bullets
        this.player.bullets.forEach(bullet=>{
            if(bullet.x != -1000){
                if(bullet.y < -100 || bullet.y > game.height + 100 || bullet.x > game.width + 100 || bullet.x < -100){
                    bullet.x = -1000;
                    bullet.body.velocity.setTo(0,0);
                }
                else{
                    this.enemys.forEach(enemy => {
                        game.physics.arcade.overlap(bullet, enemy, ()=>{
                            enemy.hp -= bullet.damage;
                            game.shotAudio.play();

                            if(enemy.hp <= 0) this.createEnemyEmitter(enemy.x, enemy.y);
                            else this.createBulletEmitter(bullet.x, bullet.y - 20);

                            bullet.x = -1000;
                            bullet.body.velocity.setTo(0,0);
                        });

                        for(let i=0; i<this.player.skill.length; i++){
                            var skill = this.player.skill[i];
                            if(skill.x != -1000){
                                game.physics.arcade.overlap(skill, enemy, ()=>{
                                    enemy.hp -= 5;

                                    if(enemy.hp <= 0) this.createEnemyEmitter(enemy.x, enemy.y);
                                    else this.createBulletEmitter(enemy.x, enemy.y + 20);

                                });
                            }
                        }
                    });

                    if(this.boss){
                        game.physics.arcade.overlap(bullet, this.boss, ()=>{
                            this.boss.hp -= bullet.damage;
                            game.shotAudio.play();

                            if(this.boss.hp <= 0) this.createEnemyEmitter(this.boss.x, this.boss.y);
                            else this.createBulletEmitter(bullet.x, bullet.y - 20);

                            bullet.x = -1000;
                            bullet.body.velocity.setTo(0,0);
                        });

                        for(let i=0; i<this.player.skill.length; i++){
                            var skill = this.player.skill[i];
                            if(skill.x != -1000){
                                game.physics.arcade.overlap(skill, this.boss, ()=>{
                                    this.boss.hp -= 5;

                                    if(this.boss.hp <= 0) this.createEnemyEmitter(this.boss.x, this.boss.y);
                                    else this.createBulletEmitter(this.boss.x, this.boss.y + 20);

                                });
                            }
                        }

                    }
                }
            }

        });

        //enemy bullets
        this.enemyBullets.forEach(bullet=>{
            if(bullet.x != -1000){
                if(bullet.y < -100 || bullet.y > game.height + 100 || bullet.x > game.width + 100 || bullet.x < -100){
                    bullet.x = -1000;
                    bullet.body.velocity.setTo(0,0);
                }
                else{
                    game.physics.arcade.overlap(bullet, this.player, ()=>{
                        this.player.hp -= bullet.damage;

                        if(this.player.hp <= 0) this.createPlayerEmitter(this.player.x, this.player.y);
                        else this.createBulletEmitter(bullet.x, bullet.y + 20);

                        bullet.x = -1000;
                        bullet.body.velocity.setTo(0,0);
                    });

                    game.physics.arcade.overlap(bullet, this.player2, ()=>{
                        this.player2.hp -= bullet.damage;

                        if(this.player2.hp <= 0) this.createPlayerEmitter(this.player2.x, this.player2.y);
                        else this.createBulletEmitter(bullet.x, bullet.y + 20);

                        bullet.x = -1000;
                        bullet.body.velocity.setTo(0,0);
                    });
                    
                    for(let i=0; i<this.player.skill.length; i++){
                        var skill = this.player.skill[i];
                        if(skill.x != -1000){
                            game.physics.arcade.overlap(skill, bullet, ()=>{
                                bullet.x = -1000;
                                bullet.body.velocity.setTo(0,0);
                            });
                        }
                    }
                    
                }
            }
        });

        //player skill
        for(let i=0; i<this.player.skill.length; i++){
            var skill = this.player.skill[i];
            if(skill.x != -1000){
                if(skill.y < -20){
                    skill.body.velocity.setTo(0,0);
                    skill.x = -1000;
                }
            }
        }

    },

    //Emitter
    createBulletEmitter:function(x,y){
        this.bulletEmitter = game.add.emitter(x,y,7);
        this.bulletEmitter.makeParticles('emitter');
        this.bulletEmitter.setYSpeed(-150, 150);
        this.bulletEmitter.setXSpeed(-150, 150);
        this.bulletEmitter.setScale(0.4, 0, 0.4, 0, 800);
        this.bulletEmitter.gravity = 500;
        this.bulletEmitter.start(true, 800, null, 15);

    },

    createPlayerEmitter: function(x,y){
        var playerEmitter = game.add.emitter(x,y,20);
        playerEmitter.makeParticles('player_emitter');
        playerEmitter.setXSpeed(-180, 180);
        playerEmitter.setYSpeed(-180, 180);
        playerEmitter.setScale(0.5, 0, 0.5, 0, 400);
        playerEmitter.start(true, 400, null, 15);
    },

    createEnemyEmitter:function(x,y){
        var enemyEmitter = game.add.emitter(x,y,20);
        enemyEmitter.makeParticles('enemy_emitter');
        enemyEmitter.setXSpeed(-180, 180);
        enemyEmitter.setYSpeed(-180, 180);
        enemyEmitter.setScale(0.5, 0, 0.5, 0, 400);
        enemyEmitter.start(true, 400, null, 15);
    },

    //enemy
    enemySetting: function(){
        this.enemys = [];
        this.enemyBullets = [];
        this.enemys.push(this.createEnemy());
        
    },

    createEnemy: function(type = 0){
        var enemy;

        if(type == 0){
            enemy = game.add.sprite(Math.random()* game.width * 0.8, -40, 'enemy');
        }
        else{
            enemy = game.add.sprite(Math.random()* game.width * 0.8, -40, 'enemy2');
        }

        enemy.type = type;
        enemy.scale.setTo(1.1,1.1);
        enemy.anchor.setTo(0.5,0.5);
        enemy.animations.add('enemy_move', [0,1,2,3],8,true);

        if(type == 0){
            enemy.scores = 100;
            enemy.maxhp = 100;
            enemy.hp = 100;

            enemy.hpBar = game.add.image(enemy.x, enemy.y + enemy.height/2, 'hp');
            enemy.hpBar.scale.setTo(0.8,0.1);
            enemy.hpBar.anchor.setTo(0.5,0.5);
        }
        else{
            enemy.scores = 300;
            enemy.maxhp = 300;
            enemy.hp = 300;

            enemy.hpBar = game.add.image(enemy.x, enemy.y + enemy.height/2, 'hp');
            enemy.hpBar.scale.setTo(0.8,0.1);
            enemy.hpBar.anchor.setTo(0.5,0.5);
        }

        game.physics.enable(enemy, Phaser.Physics.ARCADE);
        
        return enemy;
    },

    enemyMove: function(){
        var allKilled = true;
        for(let i=0; i<this.enemys.length; i++){
            let enemy = this.enemys[i];

            if(!enemy.killed){
                allKilled = false;
                enemy.animations.play('enemy_move');
                this.enemyHPcontrol(enemy);
                this.enemyAttack(enemy);

                if(!enemy.moved){
                    let rv_move = 200 + Math.random() * 400;
                    let rv_stay = 1000 + Math.random() * 2000;

                    enemy.moved = true;
                    enemy.attackable = false;
                    game.physics.arcade.moveToXY(enemy, enemy.width/2 + Math.random()*game.width*0.8, enemy.height/2 + Math.random()*game.height/4, 0, rv_move);
                    setTimeout(()=>{
                        enemy.body.velocity.setTo(0,0);
                        enemy.attackable = true;
                        setTimeout(()=>{
                            enemy.moved = false;
                        },rv_stay);
                    },rv_move);
                }
            }
        }
        if(allKilled && !this.creating && this.boss.killed){
            this.creating = true;
            setTimeout(()=>{
                this.creating = false;
                var rv = Math.random()*100;
                this.enemys = [];

                var num ;
                var counter = 0;
                if(game.scores < 1000){
                    if(rv < 50){
                        num = 2;
                    }
                    else{
                        num = 4;
                    }
                    for(var i=counter; i<num ; i++){
                        this.enemys.push(this.createEnemy());
                    }
                }
                else{
                    if(!this.player2){
                        this.player2 = game.add.sprite(this.player.x + this.player.width, this.player.y + this.player.height, 'player');
                        this.player2.anchor.setTo(0.5,0.5);
                        this.player2.scale.setTo(0.7,0.7);
                        this.player2.animations.add('player_move',[0,1,2,1,0], 8, true);
                    }
                    if(rv < 50){
                        num = 2;
                        for(var i=counter; i<num ; i++){
                            var a = Math.random()*100;
                            if(a < 50)
                                this.enemys.push(this.createEnemy(0));
                            else 
                                this.enemys.push(this.createEnemy(1));
                        }
                    }
                    else if(rv < 90){
                        num = 4;
                        for(var i=counter; i<num ; i++){
                            var a = Math.random()*100;
                            if(a < 50)
                                this.enemys.push(this.createEnemy(0));
                            else 
                                this.enemys.push(this.createEnemy(1));
                        }
                    }
                    else{
                        this.boss.killed = false;
                        this.boss.hp = this.boss.maxhp;
                        this.boss.x = game.width/2;
                        this.boss.y = -100;
                        this.boss.body.velocity.y = 300;
                    }
                }

            },3000);

        }

    },

    enemyAttack: function(enemy){
        if(enemy.attackable){
            let damage;
            if(enemy.type==0){
                damage = 50;
            }
            else{
                damage = 100;
            }

            enemy.attackable = false;
            var rv = Math.random()*100;

            if(rv < 30){

            }
            else if(rv < 90){
                for(var i = 1; i<=6; i++){
                    if(!this.recycleBullet(enemy, 1, -120+ i*40 , 140+Math.random()*100, damage)){
                        this.enemyBullets.push(this.createBullet(enemy, 1, -120+ i*40, 140+Math.random()*100, damage));
                    }
                }
                setTimeout(()=>{
                    for(var i = 1; i<=6; i++){
                        if(!this.recycleBullet(enemy, 1, -120+ i*40 , 140+Math.random()*100, damage)){
                            this.enemyBullets.push(this.createBullet(enemy, 1, -120+ i*40, 140+Math.random()*120, damage));
                        }
                    }
                },300);
            }
            else{

            }
        }
    },



    enemyHPcontrol: function(enemy){
        enemy.hpBar.x = enemy.x;
        enemy.hpBar.y = enemy.y + enemy.height/2;

        var hp = enemy.hp/enemy.maxhp * 0.8;
        hp = (hp<0)? 0: hp;

        enemy.hpBar.scale.setTo(hp,0.1);

        if(hp <= 0 && !enemy.killed){
            enemy.kill();
            enemy.killed = true;
            
            game.scores += enemy.scores;
            game.scoresText.setText("scores: " + game.scores.toString());
        }
    },

    createBoss: function(){
        this.boss = game.add.sprite(game.width/2,-100,'boss');
        this.boss.anchor.setTo(0.5,0.5);
        this.boss.scale.setTo(0.8,0.8);

        this.boss.hp = 3000;
        this.boss.maxhp = 3000;
        this.boss.scores = 3000;
        this.boss.killed = true;
        this.boss.counter = 0;
        this.boss.parameter = 70;

        this.boss.hpBar = game.add.sprite(this.boss.x, this.boss.y + this.boss.height/2, 'hp');
        this.boss.hpBar.anchor.setTo(0.5,0.5);

        game.physics.enable(this.boss, Phaser.Physics.ARCADE);
        this.boss.x = -10000;
    },

    bossMove: function(){
        if(!this.boss.killed){
            if(this.boss.y > 100)
                this.boss.body.velocity.setTo(0,0);

            this.bossHpControl();
            this.bossAttack();
        }

        
    },

    bossAttack: function(){
        var damage = 100;
        
        this.boss.counter++;
        
        if(this.boss.counter < this.boss.parameter - 50)
            for(var i = 1; i<=6; i++){
                var v_x = Math.sin(this.boss.counter+i/6 * 2*Math.PI);
                var v_y = Math.cos(this.boss.counter+i/6 * 2*Math.PI);
                if(!this.recycleBullet(this.boss, 1, 100 * v_x , 100 * v_y, damage)){
                    this.enemyBullets.push(this.createBullet(this.boss, 1, 100 * v_x , 100 * v_y, damage));
                }
            }
        else if(this.boss.counter > this.boss.parameter){
            this.boss.parameter += 20;
            this.boss.counter = this.boss.parameter - 70;
        }
    },

    bossHpControl: function(){
        this.boss.hpBar.x = this.boss.x;
        this.boss.hpBar.y = this.boss.y + this.boss.height/2;

        var hp = this.boss.hp/this.boss.maxhp * 4;
        hp = (hp<0)? 0: hp;

        this.boss.hpBar.scale.setTo(hp,0.1);

        if(hp <= 0 && !this.boss.killed){
            this.boss.x = -10000;
            this.boss.killed = true;
            
            game.scores += this.boss.scores;
            game.scoresText.setText("scores: " + game.scores.toString());

            this.player.damage += 10;
            this.player.skillnum += 1;
            if(game.gamemode == 2){
                this.player2.damage += 10;
            }
        }
    }

}