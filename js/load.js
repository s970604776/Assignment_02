var load = {
    preload: function(){
        game.scale.pageAlignHorizontally = true; 
        game.scale.pageAlignVertically = true;
        document.body.style.backgroundColor = 'black';
        if (game.device.desktop){
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            game.scale.setMinMax(game.width/2, game.height/2, game.width*2, game.height*2);
        }

        game.load.image('background','assets/img/background.png');
        game.load.image('hp','assets/img/hpBar.png');
        game.load.image('playerBullet','assets/img/bullet.png');
        game.load.image('enemyBullet','assets/img/bullet2.png');
        game.load.image('emitter','assets/img/emitter.png');
        game.load.image('player_emitter','assets/img/player_emitter.png');
        game.load.image('enemy_emitter','assets/img/enemy_emitter.png');
        game.load.image('boss','assets/img/boss.png')

        game.load.spritesheet('player','assets/img/player.png',62,48);
        game.load.spritesheet('enemy','assets/img/enemy.png', 39,60);
        game.load.spritesheet('enemy2','assets/img/enemy2.png', 32,45);
        game.load.spritesheet('button','assets/img/button.png',100,50);
        game.load.spritesheet('updown','assets/img/updown.png',39,38);

        game.load.audio('bgm','assets/audio/bgm.mp3');
        game.load.audio('btnAudio','https://raw.githubusercontent.com/photonstorm/phaser-examples/master/examples/assets/audio/SoundEffects/numkey.wav');
        game.load.audio('shot','https://raw.githubusercontent.com/photonstorm/phaser-examples/master/examples/assets/audio/SoundEffects/shot1.wav');
    },

    create: function(){
        game.stage.backgroundColor = "#00c1d4";
        game.bgm = game.add.audio('bgm');
        game.bgm.loop = true;
        game.bgm.play();
        game.btnAudio = game.add.audio('btnAudio');
        game.shotAudio = game.add.audio('shot');

        game.volume = 100;
    },

    update: function(){
        game.state.start('menu');
    }
}